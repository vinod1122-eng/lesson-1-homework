package additional.task.one;

public class Main {
    public static void main(String[] args) {
        Network vodafone = new Network("Vodafone");

        Phone iphone = new Phone("+380999990099", "Apple", "iPhone 15 Plus");
        iphone.registerInNetwork(vodafone);
        Phone iphone2 = new Phone("+380999990098", "Apple", "iPhone 14 Plus");
        iphone2.registerInNetwork(vodafone);

        iphone.makeCall("+380999990098");
    }
}
