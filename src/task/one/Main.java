package task.one;

public class Main {
    public static void main(String[] args) {
        Product product1 = new Product("Apple iPhone 12", 44999.99, "This is iPhone 12");
        Product product2 = new Product("Apple iPhone 13", 54999.99, "This is iPhone 13");
        Product product3 = new Product("Apple iPhone 14", 64999.99, "This is iPhone 14");
        System.out.println(product1);
        System.out.println(product2);
        System.out.println(product3);
    }
}
